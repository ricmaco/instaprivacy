class: center

<img src="imgs/linuxvar.png" alt="linuxvar logo" width="70%" />
# Instagram: #instaprivacy
.subtitle[.emph[Riccardo Macoratti]]
.subsubtitle[LinuxVar - LUG della provincia di Varese]
.footnote[
  Contattami a [r.macoratti@gmx.co.uk](mailto:r.macoratti@gmx.co.uk) ·
  Visita [ricma.co](https://ricma.co) ·
  Visita [linuxvar.it](http://linuxvar.it)
]

---

class: center, middle

## Social network

---

.left-column[
  ## Social network
  .selected[
    ### Intro
  ]
  ### Instagram
]
.right-column[
  <img src="imgs/social-networks.png" alt="social networks logos" width="100%"
  />
]

--

.right-column-a[
  **Cosa sono?**

  Servizi nei quali è possibile stringere molti tipi di rapporti con altri
  iscritti.
]

--

.right-column-a[
  **Quante persone usano uno di questi *social network*?**

| Social network | Mln. di utenti<sup>.emph[*]</sup> |  
| --- | :---: |
| Facebook | 2061 |
| WhatsApp | 1300 |
| Instagram | 600 |
| Tumblr | 328 |
]

.footnote[
  .emph[*]Dati presi da
  [Statista](https://www.statista.com/statistics/272014/global-social-networks-ranked-by-number-of-users/)
  , aggiornati a settembre 2017.
]

---

.left-column[
  ## Social network
  ### Intro
  .selected[
    ### Instagram
  ]
]
.right-column[
  Concentriamoci su Instragram.
  .center[
    <img src="imgs/instagram.png" alt="social networks logos" />
  ]

  La piattaforma nasce solo nel **2010**.

  È di proprietà di **Facebook<sup>.emph[*]</sup>** (che l'ha comprata per
  **$1.000.000.000**).
]

.footnote[
  .emph[*]Ricordiamo che Facebook possiede anche Whatsapp.
]

--

.right-column-a[
  Serve per condividere **foto e video** **pubblicamente** oppure privatamente.
]

---

class: center, middle

## Instagram

---

.left-column[
  ## Instagram
  .selected[
    ### Foto
  ]
  ### Selfie
  ### Stories
  ### Rimozione di contenuti
]

.right-column[
  .center[
    Come si passa il tempo su Instagram?
  ]
]

---

.img-1[
  <img src="imgs/cat.jpg" alt="cat" />
  Dopo aver visto le foto dei gattini...
]

--

.img-2[
  <img src="imgs/panorama.jpg" alt="panorama" />
  Magari qualche panorama...
]

--

.img-3[
  <img src="imgs/food.jpg" alt="pasta dish" />
  O forse un bel piatto di tortellini...
  .small[.greyout[(foto presa da [qui](https://www.instagram.com/tortelliniandco/))]]
]

---

.left-column[
  ## Instagram
  ### Foto
  .selected[
    ### Selfie
  ]
  ### Stories
  ### Rimozione di contenuti
]

.right-column[
  .center[
    Magari si pubblica qualche foto...
  ]
]

---

.img-1[
  <img src="imgs/selfie.jpg" alt="cat" />
  Un selfie?
]

--

.img-2[
  <img src="imgs/drunk.jpg" alt="panorama" />
  Un reperto di fine serata?
]

--

.img-3[
  <img src="imgs/semi-naked.jpg" alt="pasta dish" />
  Un nudo "artistico"?
  .small[.greyout[(foto presa da [qui](https://www.instagram.com/belenrodriguezreal/))]]
]

---

.left-column[
  ## Instagram
  ### Foto
  ### Selfie
  .selected[
    ### Stories
  ]
  ### Rimozione di contenuti
]

.right-column[
  .center[
    "*Ma tanto la carico nelle Stories, dopo 24 ore sparisce...*"
  ]
]

--

.right-column-a[
  .big[Mai sentito parlare di:]

  - [Instagram Stories Download](https://storiesig.com/): *webapp* per salvare
    le Stories da un qualunque *web browser*
]

--

.right-column-a[
  - [Story Saver for Instagram](https://play.google.com/store/apps/details?id=io.yoba.storysaverforinsta&hl=en):
    *app* per Android che consente di salvare le Stories dei profili che segui
]

--

.right-column-a[
  - [Ins Story Repost for Instagram](https://itunes.apple.com/us/app/ins-story-repost-for-instagram/id872899311?mt=8):
    *app* per iOS che consente di ripostare foto, video, messaggi diretti e
    Stories dei profili che segui
]

---

.left-column[
  ## Instagram
  ### Foto
  ### Selfie
  ### Stories
  .selected[
    ### Rimozione di contenuti
  ]
]

.right-column[
  .center[
    "*Al massimo cancello la foto!*"
  ]
]

--

.right-column-a[
  .big[Hai mai visto sparire qualcosa dal *web*, **veramente**?]
]

--

.right-column-a[
  - **Se qualcuno l'ha condivisa?**:
  
  Privacy Policy di Instagram<sup>.emph[†]</sup>, capitolo 3: "*[...] copies may
  remain viewable in cached and archived pages of the Service, or if other Users
  [...] have copied or saved that information*"
]

.footnote[
  .emph[†]Consultabile [qui](https://help.instagram.com/155833707900388).
]

--

.right-column-a[
  - **Se qualcuno l'ha salvata?**
  
  Abbiamo già visto che non si possono salvare solo le Stories.
]

---

class: center, middle

## E se...?

---

.left-column[
  ## E se...?
  .selected[
    ### Lavoro
  ]
  ### Diritto all'oblio
  ### Licenza
]

.right-column[
  **Facciamo finta che** tu ti sia candidato per un lavoro e il reparto risorse
  umane stia conducendo un'analisi su di te.
]

--

.right-column-a[
  Come **è abitudine** nel mondo di oggi, analizzeranno soprattutto i tuoi
  profili sui vari *social network*,
  
  .center[.big[ti piacerebbe che ci trovassero quelle foto?]]
]

---

.left-column[
  ## E se...?
  ### Lavoro
  .selected[
    ### Diritto all'oblio
  ]
  ### Licenza
]

.right-column[
  Il **diritto all'oblio** è "*il diritto a non restare indeterminatamente
  esposti ai danni ulteriori che la reiterata pubblicazione di una notizia può
  arrecare all'onore e alla reputazione*"<sup>.emph[*]</sup>.  
]

.footnote[
  .emph[*] Da [Wikipedia](https://it.wikipedia.org/wiki/Diritto_all'oblio#Italia).
]

--

.right-column-a[
  Ma alla sezione 5 della Privacy Policy di Instagram, viene spiegato che:
  "*following termination or deactivation of your account, Instagram [...] may
  retain information and User Content for a commercially reasonable time for
  backup, archival, and/or audit purposes*".

  E, molto furbescamente, non è spiegato il concetto di "*commercially
  reasonable*".
]

---

.left-column[
  ## E se...?
  ### Lavoro
  ### Diritto all'oblio
  .selected[
    ### Licenza
  ]
]

.right-column[
  E ricordiamoci che hai accettato i **termini di servizio**<sup>.emph[*]</sup>
  (TOS).
]

.footnote[
  .emph[*] Consultabili [qui](https://help.instagram.com/478745558852511).
]

--

.right-column-a[
  Perciò, quando carichi un contenuto, regali ad Instagram "*[...] a
  non-exclusive, fully paid and royalty-free, transferable, sub-licensable,
  worldwide license [...]*".

  Questo significa che anche se cancelli i contenuti, ormai la licenza ad
  Instagram l'hai data, quindi...
]

---

class: center, middle

## Caso studio

---

.left-column[
  ## Caso studio
  .selected[
    ### Tiziana Cantone
  ]
  ### Diffusione
  ### Danno
  ### Tragica fine
]

.right-column[
  .center[.big[**Chi è Tiziana Cantone?**]]
]

--

.right-column-a[
  Era una ragazza napoletana di 31 anni, che come tante altre conduceva una
  normalissima vita e "**si è trovata**" a girare una serie di video *hard* nel
  2015<sup>.emph[*]</sup>.
]

.footnote[
  .emph[*] La storia completa la trovi [qui](http://www.ilpost.it/2016/09/15/storia-tiziana-cantone/).
]

---

.left-column[
  ## Caso studio
  ### Tiziana Cantone
  .selected[
    ### Diffusione
  ]
  ### Danno
  ### Tragica fine
]

.right-column[
  Uno di questi video, ha cominciato ad essere diffuso, ovviamente **non da
  lei**, ma in un primo momento col suo consenso.

  Successivamente la diffusione è sfuggita fuori dal controllo di Tiziana.
]

--

.right-column-a[
  - All'inizio è apparso all'interno di piattaforme specializzate su contenuti
    *hard*, totalizzando visualizzazioni soprattutto nel napoletano. 
]

--

.right-column-a[
  - In seguito ha iniziato a circolare su Whatsapp, fino a diventare un
    **meme**, utilizzato anche nel video di una canzone.
]

--

.right-column-a[
  - Infine ha raggiunto la massima popolarità venendo utilizzato e condiviso
    sotto forma di *meme* numerose volte su Facebook.
]

---

.left-column[
  ## Caso studio
  ### Tiziana Cantone
  ### Diffusione
  .selected[
    ### Danno
  ]
  ### Tragica fine
]

.right-column[
  Come si può immaginare, Tiziana si è rivolta alla magistratura, che, dopo un
  processo le ha dato **parzialmente ragione**.
]

--

.right-column-a[
  Tutti i riferimenti sono sì stati cancellati, ma Tiziana avrebbe dovuto pagare
  **18.000 euro** di spese processuali, per il principio di soccombenza.
]

--

.right-column-a[
  .center[.big[Ormai il **danno sociale** è stato fatto e nessun tribunale può
  cancellarlo.]]
]

---

.left-column[
  ## Caso studio
  ### Tiziana Cantone
  ### Diffusione
  ### Danno
  .selected[
    ### Tragica fine
  ]
]

.right-column[
  Dopo la sentenza del 5 settembre 2016, Tiziana **si è suicidata** il 14
  settembre e attualmente **quattro persone sono indagate per diffamanzione**.
]

---

class: center, middle

# Cosa ti porti a casa?

Applica una semplice **regola**, quando posti qualcosa:
.big[
  **rifletti** prima di condividere qualcosa!
]

---

class: center, middle

# Grazie!

---

class: center, middle

<img src="imgs/cc.png" alt="creative commons license" width="60%" />

.left[
  Questo lavoro viene condiviso, salvo ove diversamente specificato, sotto la
  licenza [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
  International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
]
